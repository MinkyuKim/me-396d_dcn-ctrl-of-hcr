clear all
clc


clear all;
clc;


%% Initialize variables in plant model
tau1=1;
tau2=1;
tau3=1;
tau4=2;
tau5=1;
tau6=3;

gamma11=1;
gamma22=1;
gamma33=1;

beta14=0.4;
beta15=-0.15;
beta24=-0.35;
beta25=0.1;
beta32=0.1;
beta35=-0.1;
beta46=0.56;
beta56=0.2;
beta63=0.3;
beta62=-0.1;

%% Controller Initialization
%% create MPC controller object with sample time
mpc1 = mpc(mpc1_plant_C, 1);
%% specify prediction horizon
mpc1.PredictionHorizon = 5;
%% specify control horizon
mpc1.ControlHorizon = 3;
%% specify nominal values for inputs and outputs
mpc1.Model.Nominal.U = [0;0;1];
mpc1.Model.Nominal.Y = 0;
%% specify constraints for MV and MV Rate
mpc1.MV(1).Min = 0;
mpc1.MV(1).Max = 200;
mpc1.MV(1).RateMin = -15;
mpc1.MV(1).RateMax = 15;
mpc1.MV(2).Min = 0;
mpc1.MV(2).Max = 30;
mpc1.MV(2).RateMin = -3;
mpc1.MV(2).RateMax = 3;
%% specify constraints for OV
mpc1.OV(1).Min = 0;
mpc1.OV(1).Max = 1000;
%% specify weights
mpc1.Weights.MV = [0.06 0.05];
mpc1.Weights.MVRate = [0.1 0.1];
mpc1.Weights.OV = 2;
mpc1.Weights.ECR = 100000;
%% specify simulation options
options = mpcsimopt();
options.RefLookAhead = 'off';
options.MDLookAhead = 'off';
options.Constraints = 'on';
options.OpenLoop = 'off';
%% run simulation
sim(mpc1, 11, mpc1_RefSignal, mpc1_MDSignal, options);



%% plot for results
%%plot for input
figure();
for j=1:3
 subplot(3,1,j)   
 bar(simout1.Data(:,j));
end


%%plot for output
%%p lot for output
figure();
for i=1:6
    subplot(3,2,i);
plot(simout.Data(:,i),'linewidth',3);

switch i
    case 1 
        title('Experience');
    case 2 
        title('Collision rate');
    case 3
        title('velocity');
    case 4
        title('success ratio');
    case 5
        title('operation time');
    case 6
         title('Trust');
         hold on
        plot(simout1.Data(:,4),'linewidth',2,'Color','r');
end
end


