#########################################################################################
## READ ME By JM & MK 
## This code includes both system validation and model predictive control results.
## If you don't want to seperate two parts, please make sure system coefficients. 
## Because this code is sharing system specification and model, 
## you should re-defin system coefficient in case of modifying any part of this.

import numpy as np

## define a class for SCT
class SCT:
## this class contians six tanks for the modelling: each explaination in UTwiki	confluence
## gamma: coefficients for inputs 
## beta: coefficients for states
## tau: time constant
## ()_previous: previous value
## zeta: disturbance
## t: time(day)

	def experience(self, gamma11, zai1, zeta1, tau1, eta1_prev, t):
		eta1 = (gamma11*zai1 - (1-tau1)*eta1_prev + zeta1)
		eta1 = eta1/tau1
		return eta1

	def collision_rate(self, gamma22, zai2, beta62, eta6, beta32, eta3, zeta2, tau2, eta2_prev, t):
		eta2 = (gamma22*zai2 + beta62*eta6 + beta32*eta3 - (1-tau2)*eta2_prev + zeta2) 
		eta2 = eta2/tau2
		return eta2

	def speed_robot(self, gamma33, zai3, beta63, eta6, zeta3, tau3, eta3_prev, t):
		eta3 = (gamma33*zai3 + beta63*eta6 - (1-tau3)*eta3_prev + zeta3)
		eta3 = eta3/tau3
		return eta3

	def success_rate(self, beta14, eta1, beta24, eta2, zeta4, tau4, eta4_prev, t):
		eta4 = (beta14*eta1 + beta24*eta2 - (1-tau4)*eta4_prev + zeta4)
		eta4 = eta4/tau4
		return eta4

	def operating_time(self, beta15, eta1, beta25, eta2, beta35, eta3, zeta5, tau5, eta5_prev, t):
		eta5 = (beta15*eta1 + beta25*eta2 + beta35*eta3 - (1-tau5)*eta5_prev + zeta5)
		eta5 = eta5/tau5
		return eta5

	def trust_robot(self, beta46, eta4, beta56, eta5, zeta6, tau6, eta6_prev, t):
		eta6 = (beta46*eta4 + beta56*eta5 - (1-tau6)*eta6_prev + zeta6)
		eta6 = eta6/tau6
		return eta6

## define coefficients [tau, gamma, beta] 
# speed -> operating (vbeta32) : negative
# collision -> sucess rate (vbeta24) : negative
# experience -> operating time (vbeta15) : negative
# operating time -> trust (vbeta56) : negative 
# trust of robot -> collision rate (vbeta62) : negative

vtau1, vtau2, vtau3, vtau4, vtau5, vtau6 = 2, 2, 2, 2, 1, 3
vgamma11, vgamma22, vgamma33 = 1, 1, 1
vbeta62, vbeta32, vbeta63, vbeta14, vbeta24, vbeta15, vbeta25, vbeta35, vbeta46, vbeta56 = -0.1, 0.1, 0.3, 0.4, -0.35, -0.15, 0.1, -0.1, 0.56, 0.2

# define the number of data (days)
N_data = 200
time = range(N_data)

# define the state eta (1~6)
eta1 = range(N_data)
eta2 = range(N_data)
eta3 = range(N_data)
eta4 = range(N_data)
eta5 = range(N_data)
eta6 = range(N_data)

# define the initial state eta 
eta1[0] = 0
eta2[0] = 0
eta3[0] = 0
eta4[0] = 0
eta5[0] = 0
eta6[0] = 0

# define delta t (1day)
unit_tau = 1

# define all zeta are zero
no_disturb = 0
 
# define the inputs zai (1~8)
zai1 = range(N_data)
zai2 = range(N_data)
zai3 = range(N_data)

## define inputs
for i in range(0,N_data):
	# define input zai2
	if i < 14:
		zai2[i] = 0
	elif i < 20:
		zai2[i] = 5
	elif i < 26:
		zai2[i] = 0
	elif i < 30:
		zai2[i] = 5
	elif i < 35:
		zai2[i] = 0
	elif i < 40:
		zai2[i] = 5
	elif i < 45:
		zai2[i] = 0
	else:
		zai2[i] = 0

	# define input zai1	
	zai1[i] = i
	if zai1[i] > 10:
		zai1[i] = 10

	# define input zai (except 4 and 8)
	zai3[i] = 2

## Check System Model
# define Class SCT
Social_model01 = SCT()

# execute mdoel for SCT
for i in range(1,N_data):
	eta1[i] = Social_model01.experience(vgamma11, zai1[i-1], no_disturb, vtau1, eta1[i-1], i )
	eta2[i] = Social_model01.collision_rate(vgamma22, zai2[i-1], vbeta62, eta6[i-1], vbeta32, eta3[i-1], no_disturb, vtau2, eta2[i-1], i)
	eta3[i] = Social_model01.speed_robot(vgamma33, zai3[i-1], vbeta63, eta6[i-1], no_disturb, vtau3, eta3[i-1], i)
	eta4[i] = Social_model01.success_rate(vbeta14, eta1[i-1], vbeta24, eta2[i-1], no_disturb, vtau4, eta4[i-1], i)
	eta5[i] = Social_model01.operating_time(vbeta15, eta1[i-1], vbeta25, eta2[i-1], vbeta35, eta3[i-1], no_disturb, vtau5, eta5[i-1], i)
	eta6[i] = Social_model01.trust_robot(vbeta46, eta4[i-1], vbeta56, eta5[i-1], no_disturb, vtau6, eta6[i-1], i)	

# plotting data: inputs and states
import matplotlib.pyplot as plt
# figure 1: input data for validating the model 
plt.figure(1)
plt.subplot(131)
plt.plot(zai1)
plt.xlabel('time')
plt.ylabel('xi1')
plt.grid(True)

plt.subplot(132)
plt.plot(zai2)
plt.xlabel('time')
plt.ylabel('xi2')
plt.grid(True)

plt.subplot(133)
plt.plot(zai3)
plt.xlabel('time')
plt.ylabel('xi3')
plt.grid(True)

# figure 2: states for validating the model
plt.figure(2)
plt.subplot(221)
plt.plot(eta3)
plt.xlabel('time')
plt.ylabel('eta3')
plt.grid(True)

plt.subplot(222)
plt.plot(eta4)
plt.xlabel('time')
plt.ylabel('eta4')
plt.grid(True)

plt.subplot(223)
plt.plot(eta5)
plt.xlabel('time')
plt.ylabel('eta5')
plt.grid(True)

plt.subplot(224)
plt.plot(eta6)
plt.xlabel('time')
plt.ylabel('eta6')
plt.grid(True)


## start Model Predictive Controller
## We are using cvxpy, thus, please make sure to install cvxpy.
from cvxpy import *

# define state variable & input & output
A = np.zeros((6,6),float)
B = np.zeros((6,3),float)
C = np.eye(6)

# Transformation of system description in form of Matrix
A[0,0] = 1 - 1/float(vtau1)
A[1,1] = 1 - 1/float(vtau2)
A[1,2] = float(vbeta32)/float(vtau2)
A[1,5] = float(vbeta62)/float(vtau2)
A[2,2] = 1 - 1/float(vtau3)
A[2,5] = float(vbeta63)/float(vtau3)
A[3,0] = float(vbeta14)/float(vtau4) 
A[3,1] = float(vbeta24)/float(vtau4)
A[3,3] = 1 - 1/float(vtau4)
A[4,0] = float(vbeta15)/float(vtau5)
A[4,1] = float(vbeta25)/float(vtau5)
A[4,2] = float(vbeta35)/float(vtau5)
A[4,4] = 1 - 1/float(vtau5)
A[5,3] = float(vbeta46)/float(vtau6)
A[5,4] = float(vbeta56)/float(vtau6)
A[5,5] = 1 - 1/float(vtau6)

B[0,0] = float(vgamma11)/float(vtau1)
B[1,1] = float(vgamma22)/float(vtau2)
B[2,2] = float(vgamma33)/float(vtau3)

#x = Variable(6,N_data)
x_res = np.zeros((6,N_data),float)		# state results
x_des = np.array([0 ,0, 0, 0, 0, 10])	# desired state 

# constriants for states
x_min = np.array([0, -100, 0, 0, -100, 0])
x_max = np.array([1000, 1000, 1000, 1000, 500, 1000])

u_res = np.zeros((3,N_data),float)		# input results
u_des = np.zeros((3, N_data+1), float)	# desired input 

# constriants for inputs
u_min = np.array([0, 0, 0])			
u_max = np.array([100, 50, 50])
du_min = np.array([-10,-10,-10])
du_max = np.array([10, 10, 10])

# horizon steps for prediction and control
P_horizon = 5 			# horizon for predection
C_horizon = 3 			# horizon for control

# weighting matrix for cost fucntion
Qx = np.zeros((6,6),float)
Qx[5,5] = 1
Qu = np.eye(3)

# calculating steps
Step = (N_data - N_data%C_horizon)/C_horizon
print(Step) 

# begin to iterate the loop
for k in range(0,Step-1):
	states = []
	x = Variable(6,P_horizon)
	u = Variable(3,P_horizon)
	x_des[5] = k

	# prediction part (optimization)
	for p in range(0,P_horizon-1):
		cost = square(x[5,p]-x_des[5])							# cost function 
		constraints = [x[:,p+1] == A*x[:,p] + B*u[:,p],			# system description
		 			x_min <= x[:,p],							# state boundary (1)					
		 			x[:,p] <= x_max,							# state boundary (2)
		 			u[1,p] == 2,								# collision constraints
		 			u_min <= u[:,p],							# input boundary (1)
		 			u[:,p] <= u_max,							# input boundary (2)
		 			u[:,p+1] - u[:,p] <= du_max,				# input variation boundary (1)
		 			u[:,p+1] - u[:,p] >= du_min ]				# input variation boundary (2)
		states.append(Problem(Minimize(cost), constraints))		# merging problem and constraints

	print k	
	prob = sum(states)
	if k==0:		# initial condition for optimation
		prob.constraints += [x[:,0] == np.zeros((6,1),float),
							x[:,1] - x[:,0] >= np.zeros((6,1),float)]

	if k>0:			# condition for continuous iteration
		prob.constraints += [x[:,0] == x_res[:,C_horizon*(k)-1],
							 u[:,0] == u_res[:,C_horizon*(k)-1] ]

	prob.solve()	# solve problem 
	print prob.status	# check status of problem and solution 

	# save and link the result data 
	x_res[:,C_horizon*k:C_horizon*(k+1)] = x.value[:,0:C_horizon]
	u_res[:,C_horizon*k:C_horizon*(k+1)] = u.value[:,0:C_horizon]

# plot input data of Model Predictive Controller
plt.figure(3)
plt.subplot(131)
plt.plot(u_res[0,:])
plt.xlabel('time')
plt.ylabel('xi1')
plt.grid(True)

plt.subplot(132)
plt.plot(u_res[1,:])
plt.xlabel('time')
plt.ylabel('xi2')
plt.grid(True)

plt.subplot(133)
plt.plot(u_res[2,:])
plt.xlabel('time')
plt.ylabel('xi3')
plt.grid(True)

# plot states controlled by the solutions
plt.figure(4)
plt.subplot(221)
plt.plot(x_res[2,:])
plt.xlabel('time')
plt.ylabel('eta3')
plt.grid(True)

plt.subplot(222)
plt.plot(x_res[3,:])
plt.xlabel('time')
plt.ylabel('eta4')
plt.grid(True)

plt.subplot(223)
plt.plot(x_res[4,:])
plt.xlabel('time')
plt.ylabel('eta5')
plt.grid(True)

plt.subplot(224)
plt.plot(x_res[5,:])
plt.xlabel('time')
plt.ylabel('eta6')
plt.grid(True)
plt.show()
